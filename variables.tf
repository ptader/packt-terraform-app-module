variable "vpc_id" {}
variable "subnets" { type = "list" }
variable "name" {}
variable "keypair" {}

variable "extra_sgs" {
  default = []
}

variable "environment" {
  default = "dev"
}

variable "instance_type" {
  type = "map"

  default = {
    dev  = "t2.nano"
    test = "t2.micro"
    prod = "t2.medium"
  }
}

variable "extra_packages" {}
variable "external_nameserver" {}
variable "instance_count" { default = 0 }
variable "iam_role" {}
